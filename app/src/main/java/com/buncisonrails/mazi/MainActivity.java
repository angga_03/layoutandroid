package com.buncisonrails.mazi;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;
    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    TextView titleTextView;
    SearchView searchView;
    ImageView notifImageView;
    ImageView person_imageView;
    String [] titles= { "Home",  "Mengikuti",  "Chats", "Friends"};
    Fragment [] fragments = {new HomeFragment(), new MengikutiFragment(), new ChatsFragment(), new FriendsFragment()};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar)findViewById(R.id.toolBar);
        titleTextView = (TextView) toolbar.findViewById(R.id.title);
        searchView = (SearchView) toolbar.findViewById(R.id.toolsearch);
        notifImageView = (ImageView) toolbar.findViewById(R.id.notif_imageView);
        person_imageView = (ImageView) toolbar.findViewById(R.id.person_imageView);
        setSupportActionBar(toolbar);


        tabLayout = (TabLayout)findViewById(R.id.tabLayout);
        viewPager = (ViewPager)findViewById(R.id.viewPager);

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), this);

        titleTextView.setText(titles[0]);

        for(int i=0; i<titles.length;i++){
            viewPagerAdapter.addFragments(fragments[i],titles[i]);

        }


        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setIcon(R.drawable.ic_home);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_person);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_chats);
        tabLayout.getTabAt(3).setIcon(R.drawable.ic_people);


        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                titleTextView.setText(titles[tab.getPosition()]);


                switch (tab.getPosition()){
                    case 0:
                        searchView.setVisibility(View.VISIBLE);
                        notifImageView.setVisibility(View.VISIBLE);
                        person_imageView.setVisibility(View.VISIBLE);
                        notifImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_notifications));
                        person_imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_person));
                        break;
                    case 1:
                        searchView.setVisibility(View.VISIBLE);
                        notifImageView.setVisibility(View.INVISIBLE);
                        person_imageView.setVisibility(View.INVISIBLE);
                        break;
                    case 2:
                        searchView.setVisibility(View.VISIBLE);
                        notifImageView.setVisibility(View.INVISIBLE);
                        person_imageView.setVisibility(View.VISIBLE);
                        person_imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_text));
                        break;
                    case 3:
                        searchView.setVisibility(View.VISIBLE);
                        notifImageView.setVisibility(View.VISIBLE);
                        person_imageView.setVisibility(View.VISIBLE);
                        notifImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_user));
                        person_imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_group));
                        break;
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }
}
